import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: () => import('@/pages/Login/index'),
      meta: {
        permissions: []
      }
    },
    {
      path: '/unauthorized',
      name: 'unauthorized',
      component: () => import('@/pages/Error/unauthorized'),
      meta: {
        permissions: []
      }
    },
    {
      path: '/index',
      name: 'index',
      component: () => import('@/pages/Contacts/index'),
      meta: {
        permissions: []
      }
    }
  ]
})
