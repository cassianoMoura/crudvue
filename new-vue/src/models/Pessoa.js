import moment from 'moment'

export default class Pessoa {
  constructor (id, nome, sexo, endereco, documento, dtNascimento, active) {
    this._id = id || null
    this._nome = nome || null
    this._sexo = sexo || null
    this._endereco = endereco || null
    this._documento = documento || null
    this._dtNascimento = dtNascimento ? moment.unix(dtNascimento) : null
    this._active = active || true
  }

  montaModel (contact) {
    this._id = contact._id
    this._nome = contact.nome
    this._sexo = contact.sexo
    this._endereco = contact.endereco
    this._documento = contact.documento
    this._dtNascimento = moment.unix(contact.dtNascimento)
    this._active = contact.active
  }

  set id (id) {
    this._id = id
  }

  get id () {
    return this._id
  }

  set nome (nome) {
    this._nome = nome
  }

  get nome () {
    return this._nome
  }

  set sexo (sexo) {
    this._sexo = sexo
  }

  get sexo () {
    return this._sexo
  }

  set endereco (endereco) {
    this._endereco = endereco
  }

  get endereco () {
    return this._endereco
  }

  set documento (documento) {
    this._documento = documento
  }

  get documento () {
    return this._documento
  }

  set dtNascimento (dtNascimento) {
    this._dtNascimento = dtNascimento
  }

  get dtNascimento () {
    return this._dtNascimento
  }

  set active (active) {
    this._active = active
  }

  get active () {
    return this._active
  }

  get json () {
    return {
      id: this._id,
      nome: this._nome,
      sexo: this._sexo,
      endereco: this._endereco,
      documento: this._documento,
      dtNascimento: moment(this._dtNascimento).unix(),
      active: this._active
    }
  }
}
