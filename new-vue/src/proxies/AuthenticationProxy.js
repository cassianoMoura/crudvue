import axios from 'axios'

axios.defaults.baseURL = process.env.BASE_URL
export default class AuthenticationProxy {
  login (user) {
    return new Promise((resolve, reject) => {
      axios.post('login', user)
        .then((response) => resolve(response.data))
        .catch((erro) => reject(erro.data) || reject(erro))
    })
  }
}
