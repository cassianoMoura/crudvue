import axios from 'axios'

axios.defaults.baseURL = process.env.BASE_URL
export default class BaseProxy {
  constructor (endpoint, token) {
    this.endpoint = endpoint
    this.headers = {headers: {authorization: token}}
  }

  findAll () {
    if (this.headers.headers.authorization) {
      return new Promise((resolve, reject) => {
        axios.get(`${this.endpoint}`, this.headers)
          .then((response) => resolve(response.data))
          .catch((erro) => reject(erro.data) || reject(erro))
      })
    }
    return false
  }

  findOneById (id) {
    if (this.headers.headers.authorization) {
      return new Promise((resolve, reject) => {
        axios.get(`${this.endpoint}/${id}`, this.headers)
          .then((response) => resolve(response.data))
          .catch((erro) => reject(erro.data) || reject(erro))
      })
    }
    return false
  }

  create (item) {
    if (this.headers.headers.authorization) {
      return new Promise((resolve, reject) => {
        axios.post(`${this.endpoint}`, item, this.headers)
          .then((response) => resolve(response.data))
          .catch((erro) => reject(erro.data) || reject(erro))
      })
    }
    return false
  }

  update (id, item) {
    if (this.headers.headers.authorization) {
      return new Promise((resolve, reject) => {
        axios.put(`${this.endpoint}/${id}`, item, this.headers)
          .then((response) => resolve(response.data))
          .catch((erro) => reject(erro.data) || reject(erro))
      })
    }
    return false
  }
}
